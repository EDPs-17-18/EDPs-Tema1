function [p0] = shoot_method(sigma, initial_guess)
    s = sigma; % sigma
    n = length(initial_guess);
    p0 = zeros(1, n);

    % Solution of the IVP du/dx = sqrt(p0^2 - 2*sigma*(exp(u) - 1))
    %     log(-((p^2 + 2 s)  (tanh^2(1/2 (c_1 sqrt(p^2 + 2 s) -    1 * sqrt(p^2 + 2 s)))    - 1))/(2 s))
    func = @(p0) log(-((p0^2 + 2*s)*(tanh(1/2 * (1/2 * sqrt(p0^2 + 2*s) - 1 * sqrt(p0^2 + 2*s)))^2 - 1))/(2*s));
    for i=1:n
        [p0(i), Fval, EXITFLAG] = fzero(func, initial_guess(i));
    end
end
